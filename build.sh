#!/bin/sh -e
gcc \
  evlog.c \
  -o evlog \
  -O2 -march=native -g \
  `pkg-config --cflags --libs elementary`
edje_cc evlog.edc -id ./img

### minimal docs!
# generate a log:
#   efl_dbeugd
# # run any efl app now and it will connect to debugd
# # list pids of debugd clients
#   efl_debug list
# # turn on event logging
#   efl_debug evlogon 1234
# # do things like render/interact
# # turn logging off
#   efl_debug evlogoff 1234
# # you will have an efl_ebug_evlog-1234.log file in ~/
# # now run:
# ./evlog ~/efl_ebug_evlog-1234.log
